#! /bin/bash
# From: https://docs.gitlab.com/ee/user/clusters/agent/install/index.html
# # 3) Register an agent with GitLab
# docker run --pull=always --rm \
#     registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
#     --agent-token=$(cat .token) \
#     --kas-address=wss://kas.gitlab.com \
#     --agent-version stable \
#     --namespace gitlab-kubernetes-agent | kubectl apply -f -

# # 4) Install the agent into the cluster
# kubectl create namespace gitlab-kubernetes-agent
# docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable \
#     generate --agent-token=$(cat .token) \
#     --kas-address=wss://kas.gitlab.com \
#     --namespace gitlab-kubernetes-agent | kubectl apply -f -

# # 5) Create the Kubernetes secret
kubectl create secret generic -n gitlab-kubernetes-agent gitlab-kubernetes-agent-token --from-literal=token=$(cat .token)

kubectl apply -f gitlab-agent/resources.yaml -n gitlab-kubernetes-agent